# [Cinnamon-WebDeveloperMenuApplet]
=====================

Cinnamon-WebDeveloperMenuApplet is an applet for cinnamon 1.6.7 and Mint 14.


***

## How to setup:

  	
#### Git Clone :

If you have Git installed you can clone the repo.

    & git clone https://bitbucket.org/infiniteshroom/cinnamon-webdevelopermenuapplet.git
    

#### Install applet :

##### For you
Copy the folder in your home.

    & cp -r cinnamon-webdevmenu-applet/webdevmenu@dh0mp5eur/ ~/.local/share/cinnamon/applets/

#### For all users

    # cp -r cinnamon-webdevmenu-applet/webdevmenu@dh0mp5eur/ /usr/share/cinnamon/applets/
    
    
### Resources :

Apache2 with PHP and MySQL Serveur and PhPMyAdmin

    # aptitude install apache2 php5 mysql-server phpmyadmin

***  
		
### Thanks!

Please feel free to contribute to this development !
    



